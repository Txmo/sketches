char msg[] = { 'h', 'e', 'l', 'l', 'o' };
const uint8_t msg_length = 5;
uint8_t msg_index = 0;

volatile uint8_t uart_data = 0;

ISR(USART_UDRE_vect)
{
  if(uart_data) {
    UDR0 = uart_data;
    uart_data = 0;
  } else {
    UCSR0B &= ~(1 << UDRIE0);
  }
}

ISR(TWI_vect)  // controller transmitter
{
  const uint8_t status = (TWSR & 0xFC);  // escape prescaler bits
  uint8_t sendStop = 0;
  switch (status) {
    case 0x08:
    case 0x10:
      PORTD |= (1 << PD7);
      // start condition send
      TWDR = 0x01 << 1;  // target address
      break;
    case 0x18:
      PORTD |= (1 << PD6);
      // sla+w has been transmitted
      TWDR = msg[msg_index];
      break;
    case 0x28:
      PORTD |= (1 << PD5);
      // data has been transmitted
      ++msg_index;
      if (msg_index >= msg_length) {
        sendStop = true;
      } else {
        TWDR = msg[msg_index];
      }
      break;
      default:
        PORTD = 0;
        uart_data = status;
        UCSR0B |= (1 << UDRIE0);
  }
  uint8_t twcr_value = (1 << TWINT) | (1 << TWEN) | (1 << TWIE);
  if (sendStop) {
    twcr_value |= (1 << TWSTO);
    PORTD |= (1 << PD4);
  }
  TWCR = twcr_value;
}

void setup() {

  TWBR = 0x48;  // Speed to 100Khz
  TWSR = 0x01;  // Prescaler to 4 => 100Khz / 4 = 25Khz

  DDRD = 0xFF;  // PD7 as output

  UBRR0 = 103; // baud rate of 9600
  UCSR0B = (1 << UDRIE0) | (1 << TXEN0);
  UCSR0C = (0b011 << UCSZ00); // 8 bit char size
  TWCR = (1 << TWEN) | (1 << TWINT) | (1 << TWIE) | (1 << TWSTA);
  sei();
}

void loop() {
}
