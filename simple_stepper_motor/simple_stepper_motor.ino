const int stepPin = 2;
const int directionPin = 5;
const int enablePin = 8;

// must be at least 1.9us
// https://www.tme.eu/Document/1dd18faf1196df48619105e397146fdf/POLOLU-2133.pdf 
const int pulseWidth = 500;

void setup() {
  // put your setup code here, to run once:

  // Eigentlich active low und deshalb sowieso enabled
  pinMode(enablePin, OUTPUT);

  pinMode(stepPin, OUTPUT);
  pinMode(directionPin, OUTPUT);

  digitalWrite(enablePin, LOW);
}

void loop() {
  // nicht nötig?
  delayMicroseconds(pulseWidth);

  for(int i = 0; i < 200; ++i) {
    digitalWrite(stepPin, HIGH);
    delayMicroseconds(pulseWidth);

    digitalWrite(stepPin, LOW);
    delayMicroseconds(pulseWidth);
  }

  // 2 sec Pause
  delay(2000);
}
