extern "C" {
  #include "modul-Ringbuffer.h"
}

char water_level[10] = {};

void enable_uart() {
  ringbufferWrite(ringbufferLoggingInstance(), '\n');
  for(uint8_t i = 0; i < 10; ++i){
    water_level[i] = 0;
  }
  UCSR0B |= (1 << UDRIE0);
}

ISR(USART_UDRE_vect) {
  uint8_t read_value;
  uint8_t read_status = ringbufferRead(ringbufferLoggingInstance(), &read_value);
  if (read_status) {
    UDR0 = read_value;
  } else {
    UCSR0B &= ~(1 << UDRIE0);
  }
}

// Not allowed/possible if it is trigger source for ADC
// ISR(TIMER0_OVF_vect) {
// }

ISR(ADC_vect) {
  // TIFR0 = 0; // clear Timer 0 interrupt flag
  TIFR1 = 0x04; // clear Timer 1 Output Compare B interrupt flag
  int uart_data = ADC; // wie geht int??? mit 8 bit, was passiert hier?

  sprintf(water_level, "%d", uart_data);

  ringbufferLogMsg(water_level, enable_uart);
}

void setup() {

  UBRR0 = 103;  // baud rate of 9600
  UCSR0B = (1 << UDRIE0) | (1 << TXEN0) | (1 << UDRIE0);
  UCSR0C = (0b011 << UCSZ00);  // 8 bit char size

  // TCCR0B = 0x05; // Prescaler of Timer 0 to 1024
  TCCR1B = 0x03; // Prescaler of Timer 1 to 64
  OCR1B = 1953; // for 1Hz ADC overall
  TCCR1A = (1 << WGM12); // CTC = Clear Timer on Compare Match

  ADMUX = (1 << REFS0); // Channel Input 0
  //ADCSRB = 0x04; // Set Trigger Source to Timer 0 Overflow
  //ADCSRB = 0x06; // Set Trigger Source to Timer 1 Overflow
  ADCSRB = 0x05; // Set Trigger Source to Timer 1 Compare Match B
  ADCSRA = (1 << ADEN) | (1 << ADATE) | (1 << ADIE) | 0x07; // Enable ADC, Enable Auto Trigger, Enable Interrupt, Set Prescaler to 128

  ringbufferLogMsg("START", enable_uart);
  sei();
}

void loop() {
}