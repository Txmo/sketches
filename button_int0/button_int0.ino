uint8_t led_status = 0;  // 0 = off, 1 = on

ISR(INT0_vect) {
  led_status ^= 1;  // toggle between on and off
  uint8_t current_portd = PD7;
  if (led_status) {
    PORTD = current_portd | (1 << PD7);
  } else {
    PORTD = current_portd & ~(1 << PD7);
  }
}

void setup() {
  EICRA = 0x02;  // rising edge of INT0 generates interrupt request
  EIMSK = 0x01;  // enable INT0 interrupt request

  DDRD = (1 << PD7);  // PD7 as output
  sei();
}

void loop() {
}
