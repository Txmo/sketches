extern "C" {
#include "modul-Ringbuffer.h"
}

char data_msg[] = { 'D', 'R', ':', 'X', 0 };
uint8_t readWriteBit = 0;
int sensor_data = 'T';  // TODO implement ADC ISR

uint8_t target_address = 0x02;  // acutal value shifted by 1, e.g. 1 would become 2 and 2 would become 4
uint8_t own_address = 0x04;     // actual value shifted by 1

char water_level[10] = {};

void logMessage(const char *msg) {
  for (uint8_t i = 0; msg[i] != 0; ++i) {
    // Status is ignored at this point
    ringbufferWrite(ringbufferLoggingInstance(), msg[i]);
  }
  ringbufferWrite(ringbufferLoggingInstance(), '\n');
  ringbufferWrite(ringbufferLoggingInstance(), '\r');

  // Enable Data Register Empty Interrupt, as new message has been pushed to the buffer
  UCSR0B |= 1 << UDRIE0;
}

void debug(const char *msg) {
  logMessage(msg);
}

ISR(ADC_vect) {
  // ADC ist ein "shortcut" für ADCL und ADCH lesen
  sensor_data = ADC;  // wie geht int??? mit 8 bit, was passiert hier?

  sprintf(water_level, "%d", sensor_data);

  logMessage(water_level);
}

ISR(INT0_vect) {
  // start controller transmit
  TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN) | (1 << TWIE);
  readWriteBit = 0;  // 0 = Write
  logMessage("S_WRITE");
}

ISR(INT1_vect) {
  // start controller receive
  TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN) | (1 << TWIE);
  readWriteBit = 1;  // 1 = Write
  logMessage("S_READ");
}

ISR(USART_UDRE_vect) {
  uint8_t valueToLog;
  uint8_t success = ringbufferRead(ringbufferLoggingInstance(), &valueToLog);

  if (success) {
    UDR0 = valueToLog;
  } else {
    // Disable Data Register Empty Interrupt, as reading was not successful
    UCSR0B &= ~(1 << UDRIE0);
  }
}

ISR(TWI_vect) {
  logMessage("TWI_VECT");
  debug("TWI_VECT_DEBUG");
  const uint8_t status = (TWSR & 0xFC);  // escape prescaler with 0xFC
  uint8_t sendTwint = 0;
  uint8_t sendAck = 0;
  uint8_t sendStop = 0;
  uint8_t sendStart = 0;
  uint8_t received_data = 0;

  switch (status) {
    // begin controller
    // begin controller transmitter
    case 0x08:  // START Condition transmitted
    case 0x10:  // repeated START Condition transmitted
      sendTwint = 1;
      TWDR = target_address | readWriteBit;  // Address + R/W is LSB: WRITE = 0, READ = 1
      debug("START");
      break;
    case 0x18:  // Target Address + W transmitted
      debug("ADDRESS+W TX");
      TWDR = sensor_data;  // Write Byte
      data_msg[3] = sensor_data;
      logMessage(data_msg);
      sendTwint = 1;
      break;
    case 0x20:  // Target Address + W transmitted, NOT ACK received
      debug("ADDRESS+W TX NACK");
      sendStop = 1;  // STOP condition and TWSTO Flag will be resetted
      sendTwint = 1;
      break;
    case 0x28:  // Data transmitted
      debug("DATA TX");
      sendStop = 1;  // Currently only 1 Byte will be transmitted
      sendTwint = 1;
      break;
    case 0x30:  // Data transmitted, NOT ACK received
      debug("DATA TX NACK");
      sendStop = 1;  // STOP condition and TWSTO Flag will be resetted
      sendTwint = 1;
      break;
    // Target address + R has same status as address + W
    case 0x38:  // Arbitration lost in Target Address + R/W or Data Byte transmission
      debug("ARB LOST");
      sendStart = 1;  // Start condition will be sent when bus becomes free
      sendTwint = 1;
      break;
    // begin controller receiver
    // Target address + R has same status as address + W
    case 0x40:  // Target Address + R has been transmitted
      debug("ADDRESS + R/W? TX");
      sendAck = 1;  // data byte will be received and ACK will be returned
      sendTwint = 1;
      break;
    case 0x48:       // Target address + R transmitted, not ack received
      sendStop = 1;  // Stop condition will be sent and TWSTO will be reset
      sendTwint = 1;
      break;
    case 0x50:  // Data received
      debug("DATA RX");
      received_data = TWDR;
      data_msg[3] = received_data;
      logMessage(data_msg);
      sendAck = 1;  // data will be received and ACK will be returned
      sendTwint = 1;
      break;
    case 0x58:  // Last data received ? Data received NOT ACK returned
      debug("DATA RX NACK");
      received_data = TWDR;
      data_msg[3] = received_data;
      logMessage(data_msg);
      sendStop = 1;  // Stop condition will be transmitted and TWSTO will be reset
      sendTwint = 1;
      break;

    // begin target
    // begin target receiver
    case 0x60:  // own target address + w received
      debug("OWN ADDR+W RX");
      sendTwint = 1;
      sendAck = 1;
      break;
    case 0x68:  // Arbitration lost in SLA+R/W as Controller (we were previously controller)
      debug("ARB LOST IN ADDR+W AS CTRL");
      sendTwint = 1;
      sendAck = 1;
      break;
    case 0x70:  // General call address has been received
      debug("GEN_C RX");
      sendTwint = 1;
      sendAck = 1;
      break;
    case 0x78:  // Arbitration lost in SLA+R/W as Controller (we were previously controller)
      debug("ARB LOST IN ADDR+R/W AS CTRL");
      sendTwint = 1;  // Data will be received and ACK will be returned
      sendAck = 1;
      break;
    case 0x80:  // data has been received and can be read
      debug("DATA RX CAN BE READ");
      received_data = TWDR;
      data_msg[0] = received_data;
      logMessage(data_msg);
      sendTwint = 1;
      sendAck = 1;
      break;
    case 0x88:  // Previously addressed with Address + W; NOT ACK returned
      debug("PREV ADDR+W RX NACK RETURNED");
      sendTwint = 1;  // switch to NOT addressed receiver; own SLA will be recognized
      sendAck = 1;
      break;
    case 0x90:  // Previously addressed with general call; data recevied
      debug("PREV GEN_C DATA RX");
      sendTwint = 1;  // data will be received and ACK will be returned
      sendAck = 1;
      break;
    case 0x98:  // Previously addressed with general call; NOT ACK
      debug("PREV GEN_C NACK");
      sendTwint = 1;  // switch to not addressed target + own address will be recognized
      sendAck = 1;
      break;
    case 0xA0:  // Stop Condtion or repeated Start has been received while still addressed as target
      debug("STOP OR R_START RX WHILE TARGET");
      sendTwint = 1;  // switch to not addressed target + own address will be recognized
      sendAck = 1;
      break;
    // begin target transmitter
    case 0xA8:  // own address + R received
      debug("OWN ADDR+R RX");
      TWDR = sensor_data;
      data_msg[0] = sensor_data;
      logMessage(data_msg);
      sendTwint = 1;
      // sendAck = 1; NO ACK because we only send 1 byte atm
      break;
    case 0xB0:  // Arbitration lost in SLA+R/W as Controller (we previously were controller)
      debug("ARB LOST IN ADRR+R/W AS CTLR");
      TWDR = sensor_data;
      data_msg[0] = sensor_data;
      logMessage(data_msg);
      sendTwint = 1;
      sendAck = 1;
      break;
    case 0xB8:  // data has been transmitted, now load more data
      debug("DATA TX NOW LOAD MORE");
      TWDR = sensor_data;
      data_msg[0] = sensor_data;
      logMessage(data_msg);
      sendTwint = 1;
      // sendAck = 1; NO ACK because we only send 1 byte (should not happen atm)
      break;
    case 0xC0:  // Data byte has been sent but NOT ACK received
      debug("DATA TX BUT NACK RX");
      sendTwint = 1;  // switched to not addressed target; own address will be recognized
      sendAck = 1;
      break;
    case 0xC8:  // last data byte in TWDR has been transmitted
      debug("LAST DATA RX");
      sendTwint = 1;  // switched to not addressed target; own address will be recognized
      sendAck = 1;
      break;
    case 0xF8:  // no relevant state information
      // TODO "No TWCR action" what to do here?
      logMessage("E: 0xF8");
      break;
    case 0x00:  // Bus error due to illegal START or STOP
      logMessage("E: 0x00");
      break;
  }

  uint8_t twcr_status = (1 << TWIE) | (1 << TWEN);
  if (sendTwint) {
    twcr_status |= (1 << TWINT);
  }
  if (sendStop) {
    twcr_status |= (1 << TWSTO);
  }
  if (sendStart) {
    twcr_status |= (1 << TWSTA);
  }
  if (sendAck) {
    twcr_status |= (1 << TWEA);
  }

  TWCR = twcr_status;
}

void setup() {
  DDRB = (1 << PB4);
  PORTB = (1 << PB4);

  UCSR0B = 0b00101000;  // enable data register empty interrupt and interface
  // UCSR0B |= 0b0010 0000; // enable data register empty interrupt
  // UCSR0B |= 0b0000 1000; // transmitter enable

  UCSR0C = 0b00101110;
  // UCSR0C |= 0b0010 0000; // parity mode even
  // UCSR0C |= 0b0000 1000; // 2 stop bits
  // UCSR0C |= 0b0000 0110; // 8-bit charsize

  UBRR0L = 0b01100111;  // 103, 16MHz, 9600 Baud, U2X = 0

  logMessage("INIT_TWI_MODULE");
  // Init External Interrupt 0 and 1
  EIMSK = 0x03;        // Enable Interrupt INT0 and INT1
  EICRA = 0b00001111;  // Set interrupt sense control to rising edge for INT0 and INT1

  // standard mode => 100kbit/s
  // 100KHz => TWBR = (16_000_000 / 100_000 - 16) / 2
  // => (160 - 16) / 2
  // => 144 / 2
  // => 72
  // => 0x48
  TWBR = 0x48;         // 100khz
  TWSR = 0x01;         // prescaler 4 => 100khz / 4 = 25khz
  TWAR = own_address;  // Eingabe = 0x01 (is needed in controller and multi-controller)

  TCCR0B = 0x05;                                             // Timer Prescaler 1024
  ADMUX = (1 << REFS0);                                      // Channel Input 0
  ADCSRB = 0x04;                                             // Set Trigger Source to Timer 0 Overflow
  ADCSRA = (1 << ADEN) | (1 << ADATE) | (1 << ADIE) | 0x07;  // Enable ADC, Enable Auto Trigger, Enable Interrupt, Set Prescaler to 128

  sei();
}

void loop() {
  // put your main code here, to run repeatedly:
}
