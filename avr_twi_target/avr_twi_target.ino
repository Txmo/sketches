uint8_t uart_data = 0;

ISR(USART_UDRE_vect) {
  if (uart_data) {
    UDR0 = uart_data;
    uart_data = 0;
  } else {
    UCSR0B &= ~(1 << UDRIE0);
  }
}

ISR(TWI_vect) {
  uint8_t status = (TWSR & 0xFC);  // escape prescaler bits
  switch (status) {
    case 0x60:
      PORTD |= (1 << PD7);
      // own address + write has been received
      break;
    case 0x80:
      PORTD |= (1 << PD6);
      uart_data = TWDR;
      UCSR0B |= (1 << UDRIE0);
      // data has been received and can be read
      break;
    default:
      PORTD |= (1 << PD5);
      uart_data = status;
      UCSR0B |= (1 << UDRIE0);
  }

  TWCR = (1 << TWEN) | (1 << TWINT) | (1 << TWIE) | (1 << TWEA);
}

void setup() {
  DDRD = 0xFF;  // PD7 as output

  TWAR = 0x01;                                     // Adresse 1
  TWCR = (1 << TWEN) | (1 << TWIE) | (1 << TWEA);  // Enable twi and interrupts + auto acknowledge of own address

  UBRR0 = 103;  // baud rate of 9600
  UCSR0B = (1 << UDRIE0) | (1 << TXEN0);
  UCSR0C = (0b011 << UCSZ00);  // 8 bit char size

  uart_data = 'M';
  UCSR0B |= (1 << UDRIE0);
  sei();
}

void loop() {
  // put your main code here, to run repeatedly:
}
