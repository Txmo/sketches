/*!
  \file modul-Ringbuffer.h
  \brief Hardware-unabhängige Definitionen für das Modul-Ringbuffer.

  \details Modul Ringbuffer

  Aufgabe: Das Modul-Ringbuffer bietet eine Datenstruktur zur Verwaltung von
           16Byte Speicher. Der Ringbuffer wird ohne die Angabe von spezifischer
           Position im Speicher beschrieben und gelesen.
           Für das Lesen und Schreiben wird eine interne Lese- und Schreibposition
           genutzt. Ist der Ringbuffer "voll" wird wieder von "vorne" begonnen und
           alte Daten werden überschrieben.
           Es können nur Daten überschrieben werden, die bereits gelesen wurden.
           Es können nur Daten gelesen werden, die bereits geschrieben wurden.
           Die Leseposition kann die Schreibposition nicht überholen und die
           Schreibposition kann die Leseposition nicht "überrunden".

*/

#ifndef _MODUL_RINGBUFFER_H
#define _MODUL_RINGBUFFER_H 1

/* Importe: Beginn                                                            */

// für uint8_t
#include <inttypes.h>

/* Importe: Ende                                                              */
/*----------------------------------------------------------------------------*/
/* Modulmethoden / Definitionen: Beginn                                       */

typedef struct Ringbuffer
{
  uint8_t mem[128];
  uint8_t size;
  uint8_t readPosition;
  uint8_t writePosition;
} Ringbuffer;

/**
 * Initializes the given @c Ringbuffer
 * @param ringbuffer Ringbuffer to initialize
 */
void ringbufferNew(Ringbuffer *ringbuffer);

/**
 * Returns the @c Ringbuffer instance for UART logging purposes.
 * @return Logging Ringbuffer
 */
Ringbuffer *ringbufferLoggingInstance();

/**
 * Writes the given msg to the logging ringbuffer.
 * The callback function is called no matter what the result of the write opeartion was.
 * It is possible that only a part of the message can be written if the underlying 
 * ringbuffer is "full".
 * @param msg Message to log
 * @param cb Function to call after writing to
*/
void ringbufferLogMsg(char *msg, void (*cb)(void));

/**
 * Returns the "global" @c Ringbuffer instance.
 * @return System Ringbuffer
 */
Ringbuffer *ringbufferInstance();

/**
 * Reads a value into @p value from the given @p ringbuffer.
 * Returns 0 if the operation did not succeed and 1 if it did.
 * @param ringbuffer
 * @param value
 * @return 0 or 1
 */
uint8_t ringbufferRead(Ringbuffer *ringbuffer, uint8_t *value);

/**
 * Writes the given @p value into the given @p ringbuffer.
 * Returns 0 if the operation did not succeed and 1 if it did.
 * @param ringbuffer
 * @param value
 * @returns 0 or 1
 */
uint8_t ringbufferWrite(Ringbuffer *ringbuffer, uint8_t value);

/* Modulmethoden / Definitionen: Ende                                         */
#endif // _MODUL_RINGBUFFER_H
