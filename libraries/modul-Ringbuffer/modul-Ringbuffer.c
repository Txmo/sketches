#include "modul-Ringbuffer.h"

/* Modul-globale Variablen: Beginn                                            */

/* Modul-globale Variablen: Ende                                              */
//
/*----------------------------------------------------------------------------*/
//
/* Modulmethoden / Implementierungen: Beginn                                  */

void ringbufferNew(Ringbuffer *ringbuffer)
{
  Ringbuffer tmp = {
      .size = 128};
  *ringbuffer = tmp;
}

Ringbuffer *ringbufferLoggingInstance()
{
  static _Bool isInit = 0;
  static Ringbuffer rb;

  if (!isInit)
  {
    ringbufferNew(&rb);
    isInit = 1;
  }

  return &rb;
}

void ringbufferLogMsg(char *msg, void (*cb)(void))
{
  uint8_t status = 1;
  for (uint8_t i = 0; status != 0 && msg[i] != 0; ++i)
  {
    status = ringbufferWrite(ringbufferLoggingInstance(), msg[i]);
  }

  cb();
}

Ringbuffer *ringbufferInstance()
{
  static _Bool isInit = 0;
  static Ringbuffer rb;

  if (!isInit)
  {
    ringbufferNew(&rb);
    isInit = 1;
  }

  return &rb;
}

uint8_t ringbufferRead(Ringbuffer *ringbuffer, uint8_t *value)
{
  if (ringbuffer->readPosition != ringbuffer->writePosition)
  {
    uint8_t nextPosition = (ringbuffer->readPosition + 1) % ringbuffer->size;
    *value = ringbuffer->mem[ringbuffer->readPosition];
    ringbuffer->readPosition = nextPosition;
    return 1;
  }
  else
  {
    return 0;
  }
}

uint8_t ringbufferWrite(Ringbuffer *ringbuffer, uint8_t value)
{
  uint8_t nextPosition = (ringbuffer->writePosition + 1) % ringbuffer->size;
  if (nextPosition != ringbuffer->readPosition)
  {
    ringbuffer->mem[ringbuffer->writePosition] = value;
    ringbuffer->writePosition = nextPosition;
    return 1;
  }
  else
  {
    return 0;
  }
}

/* Modulmethoden / Implementierungen: Ende                                    */
// EOF
